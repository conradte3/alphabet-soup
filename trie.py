class TrieNode:
  """A single node for a Trie structure. Each node simply contains a dictionary that maps letters
    to child nodes, and a boolean indicated whether the nodes preceding this one create a word
  """

  def __init__(self):
    self.children = {}
    self.is_word = False

  def insert(self, word):
    # If we've got an empty string, then this is the end of the word chain
    if not word:
      self.is_word = True
    else:
      letter = word[0]

      # If there's no existing child node for this letter, create one
      if letter not in self.children:
        self.children[letter] = TrieNode()

      # Recursively insert everything after this letter into the child node
      self.children[letter].insert(word[1:])


class TrieIterator:
  """Allows one-directional traversal through a set of Trie nodes, and tracks the word
    that the current node represents
  """

  def __init__(self):
    self.root = TrieNode()
    self.node = self.root
    self.word = ''

  def insert_all(self, words, ignore_strings):
    """Inserts all words into the Trie root, first removing any strings to be ignored from those words"""
    for word in words:
      for ignore in ignore_strings:
        word = word.replace(ignore, '')

      self.root.insert(word)

  def has_next(self, letter):
    """Returns True if the current node has a child connected by the letter"""
    return letter in self.node.children

  def get_next(self, letter):
    """Returns the child connected by the letter to the current node, or None if there is no such child"""
    if self.has_next(letter):
      self.node = self.node.children[letter]
      self.word += letter
      return self.node
    else:
      return None

  def current_valid_word(self):
    """Returns the word that the current node represents if it is actually a valid
      word (is_word is true), otherwise returns an empty string"""
    if self.node.is_word:
      return self.word
    else:
      return ''

  def reset_current(self):
    """Resets the current node and word to be the root and an empty string, respectively"""
    self.node = self.root
    self.word = ''
