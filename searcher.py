class WordSearcher:
  """Given a grid of letters and words to search for, finds all words and stores their beginning
    and ending coordinates.
  """

  def __init__(self, trie, ignore_strings, len_x, len_y, grid, words):
    """Constructor for the WordSearcher class

    trie           - Structure used to determine whether letters are in words we're looking for
    ignore_strings - Strings which will not actually be in the grid even if they are in the words, so
                      should be ignored when searching
    len_x/y        - The dimensions of the grid, assumed to be correct
    grid           - A two-dimensional list of letters to search in
    words          - A list of words to search for
    """
    self.results = {}

    self.trie = trie
    self.len_x = len_x
    self.len_y = len_y
    self.grid = grid
    self.words = words

    # Set up the trie with all the words we're looking for
    self.trie.insert_all(self.words, ignore_strings)

  def perform_search(self):
    """Loops through all letters in the grid, searching for words"""

    for x in range(0, self.len_x):
      for y in range(0, self.len_y):
        self.search_from(x, y)

  def search_from(self, x, y):
    """Begins a search, starting from the specified coordinates

      Checks the letter at (x,y) to see if it's the beginning of a word. If it is, every direction from that
      point is searched to see if any words can be completed.

      x - Must be in the range [0, len_x)
      y - Must be in the range [0, len_y)
    """
    letter = self.grid[x][y]

    # Find out if this is the first letter in any of our words
    if self.trie.has_next(letter):
      # Search each direction for words
      for dir_x in range(-1, 2):
        for dir_y in range(-1, 2):
          self.search_direction(x, y, dir_x, dir_y)
  
  def search_direction(self, start_x, start_y, dir_x, dir_y):
    """Updates results with all matching words found in the specified direction from the specified starting point

      start_x/y - If these are outside of this searcher's dimensions, no search will be performed
      dir_x/y   - Function returns immediately if both directions are zero
    """

    if dir_x == 0 and dir_y == 0:
      return

    x = start_x
    y = start_y

    # Keep going until we hit an edge
    while (x >= 0 and x < self.len_x) and (y >= 0 and y < self.len_y):
      letter = self.grid[x][y]

      if self.trie.get_next(letter):
        # If we've completed a word, add it to our results with the coordinates where it ends
        valid_word = self.trie.current_valid_word()
        if valid_word:
          self.results[valid_word] = (start_x, start_y, x, y)
      else:
        # Letter didn't match any words, we're done with this direction
        break

      x += dir_x
      y += dir_y

    self.trie.reset_current()