import os.path

from trie import TrieIterator
from searcher import WordSearcher


def parse_input(file_name):
  """Parses a file which specifies dimensions, a grid of letters, and words to find
  
    File name is assumed to be valid, and file contents are assumed to be in the correct format
  """

  with open(file_name, 'r') as file:
    # First get the dimensions
    dimensions = file.readline().split('x')
    len_x = int(dimensions[0])
    len_y = int(dimensions[1])

    # Using the dimensions we can read until the end of the board
    grid = []
    for i in range(len_y):
      grid.append(file.readline().strip('\n').split(' '))

    # Now just read the rest of the file that contains the words to search for
    words = file.read().splitlines()

  return (len_x, len_y, grid, words)

def output_results(searcher):
  """Prints each word found, along with the coordinates indicating where it starts and ends"""
  for word in searcher.words:
    if word.replace(' ', '') in searcher.results:
      result = searcher.results[word.replace(' ', '')]
      print('{0} {1}:{2} {3}:{4}'.format(word, *result))
    else:
      print('{0} not found'.format(word))


def main():
  file_name = ''
  while not os.path.isfile(file_name):
    file_name = input('Please enter a valid input file name:\n')
  
  params = parse_input(file_name)

  searcher = WordSearcher(TrieIterator(), [' ', '-'], *params)
  searcher.perform_search()

  output_results(searcher)



main()